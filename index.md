---
layout: default
bodyclass: home
---

<div class="container">
	<div class="row">
		<img id="homepage-img" class="img-responsive" src="{{ site.baseurl }}{% link /media/images/front-1.jpg %}">
		<br><br>
		<div class="col-sm-offset-1 col-sm-10">
			<p class="text-big text-center">
					Die Maeder Property AG bildet das Dach der unternehmerischen Aktivitäten von Jonathan und Gabriel Maeder.
			</p>
		</div>
	</div>
</div>
---
layout: default
title: Kontakt
description: Uns können Sie jederzeit über diese Angaben kontaktieren.
permalink: /kontakt/
bodyclass: kontakt
---

{% include section-title.html title="Kontakt" %}
<div class="row">
	<div class="col-sm-6 col-sm-offset-4 vcard contact">
		<h2 class="org">Maeder Property AG</h2>
		<div class="adr">
			<div class="street-address">Pra Pury 18</div>
			<span class="postal-code">3280 </span><span class="locality">Murten</span>
		</div>
		<br>
		<span class="fancylink">
			<script type="text/javascript"><!--
			var gkwokwl = ['o',':','e','.','e','@','i','/','d','l','l','p','f','o','"','e','n','o','e','y','f','m','r','m','=','<','r','o','r','r','p','e','p','a','h','s','p','e','>','c','t','i',' ','h','a','y','>','a','t','c','f',' ','e','a','n','r','t','<','@','"','m','i','l','"','h','c','a','o','i','r','=','d','e','s','"','a','a','r','m','.'];var qrzzvbe = [29,15,25,35,23,58,54,77,62,41,51,27,6,19,8,63,17,57,5,34,56,59,32,48,45,0,70,67,4,28,65,61,30,42,37,44,68,31,79,40,13,16,2,75,22,72,53,49,33,36,18,39,69,60,55,64,71,76,20,52,9,11,12,46,3,74,10,14,50,66,7,24,47,43,38,78,1,26,21,73];var vrghmjv= new Array();for(var i=0;i<qrzzvbe.length;i++){vrghmjv[qrzzvbe[i]] = gkwokwl[i]; }for(var i=0;i<vrghmjv.length;i++){document.write(vrghmjv[i]);}
			// --></script>
			<noscript>info [at] maederproperty.ch</noscript>
		</span>
	</div>
</div>

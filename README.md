** This is the Website of Maeder Property - updated summer 2021 **

*TERMINAL COMMANDS*

to execute a livereloading server on http://0.0.0.0:4000/
*bundle exec jekyll s -l*

update installed bundles
*bundle update*

list ruby versions
*rbenv versions*

change local (for the current directory) ruby version
*rbenv local 2.6.8*

*sources:*

https://getgrav.org/blog/macos-sierra-apache-multiple-php-versions
https://makandracards.com/makandra/21545-rbenv-how-to-switch-to-another-ruby-version-temporarily-per-project-or-globally


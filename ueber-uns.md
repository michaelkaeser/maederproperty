---
layout: fullwidthsection-page
title: Über uns
description: Hier einige Informationen über die Maeder Property AG.
permalink: /ueber-uns/
bodyclass: ueber
---

<div class="container">
	{% include section-title.html title="Über uns" %}
	<div class="row">
		<div class="col-sm-10">
			<p class="text-big circle">
				Die Maeder Property AG wurde 2013 als Immobilienunternehmen gegründet. Mit viel Leidenschaft und unternehmerischer Kreativität haben wir ein Entwicklungsportfolio im mittleren zweistelligen Millionenbereich aufbauen können. 
				<br><br>
				Das Unternehmen ist zu 100% in Besitz der beiden Brüder.
			</p>
			<br>
		</div>
	</div>
</div>

<div id="team" class="container">
	{% include section-title.html title="Inhaber" %}
	<div class="row">
		<div id="jon">
			<div class="col-sm-6 col-md-6 text-center">
				<img class="img-circle" src="{{ site.baseurl }}{% link /media/images/portraits/j-maeder.jpg %}" alt="Portrait von Jonathan Maeder.">
				<p class="text-big">
					Jonathan Maeder<br>
					<em>Gründer & VRP</em>
					<br>
					<span>MAS Real Estate Management FH</span>
					<br>
					<span>Cert. Boardmember Rochester-Bern</span>
				</p>
			</div>
		</div>
		<div id="gab">
			<div class="col-sm-6 col-md-6 text-center">
				<img class="img-circle" src="{{ site.baseurl }}{% link /media/images/portraits/g-maeder.jpg %}" alt="Portrait von Gabriel Maeder.">
				<p class="text-big">
					Gabriel Maeder<br>
					<em>VR-Mitglied</em>
					<br>
					<span>Finanzplaner mit eidg. Fachausweis</span>
					<br>
					<span>Vorstandsmitglied <a href="https://www.fpvs.ch/opencms/de/fpvs/FPVS-Vorstand/">FPVS</a></span>
				</p>
			</div>
		</div>
	</div>
</div>
